# #!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
###############################################################################
# LireCouleur6 - tools to help with reading French language
#
# http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 0.0.1
#
# GNU General Public Licence (GPL) version 3
# https://www.gnu.org/licenses/gpl-3.0.en.html
###############################################################################
import uno
import unohelper
import lirecouleur.text

STYLE_CORRESPONDANCE = {
    'color': 'CharColor',
    'height': 'CharHeight',
    'weight': 'CharWeight',
    'underline': 'CharUnderline',
    'background': 'CharBackColor',
    'contour': 'CharContoured',
    'underline': 'CharUnderline',
    'shadow': 'CharShadowed',
    'emphasis': 'CharEmphasis',
    'scale_width': 'CharScaleWidth',
    'font_name': 'CharFontName',
    'font_family': 'CharFontFamily',
    'line_spacing': 'ParaLineSpacing'
    }



'''
Constitution d'un dictionnaire des traitements de texte disponibles
'''
lc6classes = {}
def register_lc6_function(cls):
    lc6classes[cls.__name__.lower()] = cls
    return cls


'''
Création d'une instance de fonction LC6
'''
def create_lc6_function(lcProf, lcParams=None):
    return lc6classes[lcProf['function']](lcProf, lcParams)

'''
Create a UNO struct and return it.
Similar to the function of the same name in OOo Basic. -- Copied from Danny Brewer library
'''
def create_uno_struct(cTypeName):
    try:
        sm = uno.getComponentContext().getServiceManager()
        oCoreReflection = sm.createInstance( "com.sun.star.reflection.CoreReflection" )
        # Get the IDL class for the type name
        oXIdlClass = oCoreReflection.forName(cTypeName)
        # Create the struct.
        __oReturnValue, oStruct = oXIdlClass.createObject(None)
        return oStruct
    except:
        return None


class FunctionLC6:
    '''
    Classe parent de tous les traitements de texte possibles
    '''
    def __init__(self, lcProf, lcParams=None):
        self._stopped = True

        self._name = lcProf['function']
        if 'format' in lcProf:
            self._mode_style = isinstance(lcProf['format'], list)
            self._styles = [k for k in lcProf['format']]
            self._format = lcProf['format']
        self._params = lcParams
        if self._params is None:
            self._params = {}
        if not 'SYLLABES_ECRITES' in self._params:
            self._params['SYLLABES_OF'] = lirecouleur.text.SYLLABES_ECRITES
        else:
            if self._params['SYLLABES_ECRITES']:
                self._params['SYLLABES_OF'] = lirecouleur.text.SYLLABES_ECRITES
            else:
                self._params['SYLLABES_OF'] = lirecouleur.text.SYLLABES_ORALES
        if not 'SYLLABES_STD' in self._params:
            self._params['SYLLABES_SL'] = lirecouleur.text.SYLLABES_LC
        else:
            if self._params['SYLLABES_STD']:
                self._params['SYLLABES_SL'] = lirecouleur.text.SYLLABES_STD
            else:
                self._params['SYLLABES_SL'] = lirecouleur.text.SYLLABES_LC

    def is_stopped(self):
        return self._stopped

    def run(self, xParagraph, xContext):
        self._stopped = False
        self.execute(xParagraph, xContext)
        self._stopped = True

    def execute(self, xParagraph, xContext):
        '''
        Point d'entrée pour l'exécution de la fonction de traitement de texte
        '''
        pass

    def stop(self):
        '''
        Ordre d'arrêt du de la fonction
        '''
        self._stopped = True

    def set_style(self, xCursor, styl):
        if self._mode_style:
            ''' Application du format sans style de caractère '''
            for func in styl:
                if func in STYLE_CORRESPONDANCE:
                    if func == 'line_spacing':
                        pls = create_uno_struct('com.sun.star.style.LineSpacing')
                        pls.Mode = 0
                        pls.Height = eval(styl[func])
                        xCursor.setPropertyValue(STYLE_CORRESPONDANCE[func], pls)
                    else:
                        fff = STYLE_CORRESPONDANCE[func]
                        val = styl[func]
                        try:
                            val = eval(styl[func])
                        except:
                            pass
                        try:
                            xCursor.setPropertyValue(fff, val)
                        except:
                            pass
        else:
            ''' Application d'un format défini par un style de caractère '''
            xCursor.setPropertyValue('CharStyleName', styl)


@register_lc6_function
class Defaut(FunctionLC6):
    '''
    Présentation du texte avec l'affichage par défaut (noir)
    '''
    def __init__(self, lcProf, lcParams):
        FunctionLC6.__init__(self, lcProf, lcParams)

    def execute(self, xParagraph, _xContext):
        ll = len(xParagraph.getString())
        xParagraph.collapseToStart()
        xParagraph.goRight(ll, True)
        xParagraph.setPropertyToDefault('CharStyleName')
        xParagraph.setPropertyToDefault('CharBackColor')
        xParagraph.setPropertyToDefault('CharColor')
        xParagraph.setPropertyToDefault('CharWeight')


@register_lc6_function
class Phonemes(FunctionLC6):
    '''
    Présentation des phonèmes selon les styles de caractères
    qui leur sont attribués
    '''
    def __init__(self, lcProf, lcParams):
        FunctionLC6.__init__(self, lcProf, lcParams)

    def execute(self, xParagraph, _xContext):
        # décoder les phonemes du texte
        lphons = lirecouleur.text.phonemes(xParagraph.getString(),
                self._params['novice_reader'],
                self._params['SYLLABES_OF'])

        # construction du masque des phonèmes à mettre en évidence
        masquePhon = {}
        for k in self._format:
            ff = self._format[k]
            if 'phonemes' in ff:
                for ph in ff['phonemes']:
                    masquePhon[ph] = k

        # mise en évidence des phonèmes
        xParagraph.collapseToStart()
        xText = xParagraph.getText()
        for w in lphons:
            if self.is_stopped(): return
            if type(w) is list:
                ''' une liste de phonèmes '''
                for phon in w:
                    pos = len(phon[1])
                    if phon[0] in masquePhon:
                        xCursor = xText.createTextCursorByRange(xParagraph)
                        xCursor.goRight(pos, True)
                        self.set_style(xCursor, masquePhon[phon[0]])
                        del xCursor

                    xParagraph.goRight(pos, False)
            else:
                ''' une portion de texte non décodée '''
                pos = len(w)
                xParagraph.goRight(pos, False)


@register_lc6_function
class Syllabes(FunctionLC6):
    '''
    Présentation des syllabes selon une alternance de
    styles de caractères (couleurs)
    '''
    def __init__(self, lcProf, lcParams):
        FunctionLC6.__init__(self, lcProf, lcParams)

    def execute(self, xParagraph, _xContext):
        # décoder les syllabes du texte
        lsyll = lirecouleur.text.syllables(xParagraph.getString(),
                self._params['novice_reader'],
                (self._params['SYLLABES_OF'], self._params['SYLLABES_SL']))

        # recoder avec les styles de caractères
        nbstyl = len(self._styles)
        i = 0
        xParagraph.collapseToStart()
        xText = xParagraph.getText()
        for w in lsyll:
            if self.is_stopped(): return
            if type(w) is list:
                ''' une liste de syllabes '''
                for syl in w:
                    xCursor = xText.createTextCursorByRange(xParagraph)
                    pos = len(syl)
                    xCursor.goRight(pos, True)
                    self.set_style(xCursor, self._styles[i])
                    i += 1
                    i = i%nbstyl
                    del xCursor

                    xParagraph.goRight(pos, False)
            else:
                ''' une portion de texte non décodée '''
                pos = len(w)
                xParagraph.goRight(pos, False)

@register_lc6_function
class Mots(FunctionLC6):
    '''
    Présentation des mots selon une alternance de
    styles de caractères (couleurs)
    '''
    def __init__(self, lcProf, lcParams):
        FunctionLC6.__init__(self, lcProf, lcParams)

    def execute(self, xParagraph, _xContext):
        nbstyl = len(self._styles)
        xText = xParagraph.getText()

        xWordCursor = xText.createTextCursorByRange(xParagraph)
        xWordCursor.collapseToStart()
        xWordCursor.gotoStartOfWord(False)

        # placement à la fin du dernier mot du paragraphe
        xParagraph.collapseToEnd()

        i = 0
        j = 0
        while xText.compareRegionEnds(xWordCursor, xParagraph) >= 0 and j < 10000:
            if self.is_stopped(): return
            xWordCursor.collapseToStart()
            xWordCursor.gotoStartOfWord(False)
            xWordCursor.gotoEndOfWord(True)
            self.set_style(xWordCursor, self._styles[i])
            i += 1
            j += 1
            i = i%nbstyl

            # mot suivant
            xWordCursor.collapseToEnd()
            if not xWordCursor.gotoNextWord(False):
                return
            if xWordCursor.isEndOfParagraph():
                xWordCursor.gotoNextParagraph(False)

@register_lc6_function
class Lignes(FunctionLC6):
    '''
    Présentation des lignes selon une alternance de
    styles de caractères (couleurs)
    '''
    ialt = 0

    def __init__(self, lcProf, lcParams):
        FunctionLC6.__init__(self, lcProf, lcParams)

    def execute(self, xParagraph, xContext):
        nbstyl = len(self._styles)
        xText = xParagraph.getText()

        smgr = xContext.getServiceManager()
        desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop", xContext)
        xDocument = desktop.getCurrentComponent()

        xSelectionSupplier = xDocument.getCurrentController()
        xCursLi = xSelectionSupplier.getViewCursor()
        xCursLi.collapseToStart()
        xCursLi.gotoStartOfLine(False)

        # placement à la fin du dernier mot du paragraphe
        xParagraph.collapseToEnd()

        j = 0
        while xText.compareRegionEnds(xCursLi, xParagraph) >= 0 and j < 10000:
            if self.is_stopped(): return
            xCursLi.collapseToStart()
            xCursLi.gotoStartOfLine(False)
            xCursLi.gotoEndOfLine(True)
            self.set_style(xCursLi, self._styles[Lignes.ialt])
            j += 1
            if len(xCursLi.getString().encode('UTF-8')) > 0:
                Lignes.ialt = (Lignes.ialt+1)%nbstyl

            # retour au début de ligne et passage à la ligne suivante
            xCursLi.collapseToStart()
            if not xCursLi.goDown(1, False):
                return
