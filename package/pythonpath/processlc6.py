# #!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
###############################################################################
# LireCouleur6 - tools to help with reading French language
#
# http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 0.0.1
#
# GNU General Public Licence (GPL) version 3
# https://www.gnu.org/licenses/gpl-3.0.en.html
# .\soffice.exe --norestore --accept='socket,host=localhost,port=2002,tcpNoDelay=1;urp;StarOffice.ComponentContext'
###############################################################################
import uno
import unohelper
from threading import Thread
from time import sleep
from com.sun.star.awt.PosSize import POSSIZE
from com.sun.star.awt import (XActionListener)
from .userprofile import UserProfile

class ProcessLC6(unohelper.Base, XActionListener):
    '''
    Applique un profil utilisateur sur le document courant
    '''
    def __init__(self, ctx):
        self._context = ctx
        self._stopped = True
        self._ui = None

    def _create_uno_service(self, serviceName):
        sm = self._context.getServiceManager()
        try:
            return sm.createInstanceWithContext(serviceName, self._context)
        except:
            pass

        try:
            return sm.createInstance(serviceName)
        except:
            pass

    def get_context(self):
        return self._context

    def _create_ui(self):
        '''
        Création de la boite de dialogue non modale qui affiche une toupie et
        le bouton de commande de l'arrêt d'un traitement
        '''
        MARGIN = 2
        XPOSITION = 190
        YPOSITION = 107
        TOUPIE_SZ = 52
        BUTTON_WIDTH = TOUPIE_SZ
        BUTTON_HEIGHT = 20
        WIDTH = TOUPIE_SZ+2*MARGIN
        HEIGHT = MARGIN*3 + BUTTON_HEIGHT + TOUPIE_SZ
        TITLE = "LireCouleur 6"
        CANCEL = "Annuler"

        # lancement de l'animation
        ctx = self._context
        smgr = ctx.getServiceManager()
        desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)
        frame = desktop.getCurrentFrame()
        window = frame.getContainerWindow()
        toolkit = window.getToolkit()
        dialogModel = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialogModel", ctx)

        dialogModel.PositionX = XPOSITION
        dialogModel.PositionY = YPOSITION
        dialogModel.Width = WIDTH
        dialogModel.Height = HEIGHT
        dialogModel.Title = TITLE

        anim = self._create_uno_service("com.sun.star.awt.AnimatedImagesControl")
        oToupie = self._create_uno_service("com.sun.star.awt.SpinningProgressControlModel")
        anim.setModel(oToupie)
        anim.setPosSize(MARGIN, MARGIN, int(TOUPIE_SZ*2.54), int(TOUPIE_SZ*2.54), POSSIZE)

        button = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel")
        button.Width  = BUTTON_WIDTH
        button.Height = BUTTON_HEIGHT
        button.PositionX = (dialogModel.Width-button.Width)/2
        button.PositionY = dialogModel.Height-button.Height-2
        button.Label = CANCEL
        dialogModel.insertByName("cancel", button)

        self._ui = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialog", ctx);
        self._ui.setModel(dialogModel);

        self._ui.getControl("cancel").addActionListener(self)
        self._ui.addControl("toupie", anim)

        self._ui.setVisible(True)
        self._ui.createPeer(toolkit, None)

        anim.startAnimation()

    def actionPerformed(self, _actionEvent):
        '''
        Réponse au clic d'annulation : arrêt du traitement
        '''
        self._uprofile.stop()
        self._end_of_process()

    def _end_of_process(self):
        self.stop()

    def __set_profile(self, xParagraph, xLCursorLimit=None, xRCursorLimit=None):
        '''
        Application d'un profil sur le paragraphe courant
        '''
        if self.is_stopped(): return
        oText = xParagraph.getText()
        xCursor = oText.createTextCursorByRange(xParagraph)
        if not xLCursorLimit is None:
            if oText.compareRegionStarts(xLCursorLimit, xCursor) <= 0:
                ''' prendre la portion de fin '''
                xCursor.collapseToEnd()
                xCursor.gotoRange(xLCursorLimit, True)
        if not xRCursorLimit is None:
            if oText.compareRegionEnds(xCursor, xRCursorLimit) <= 0:
                ''' prendre la portion de début '''
                xCursor.collapseToStart()
                xCursor.gotoRange(xRCursorLimit, True)

        ''' création boite de dialogue et lancement exécution '''
        self._uprofile.run(xCursor, self._context)

        del xCursor

    def __is_anything_selected(self, xIndexAccess):
        '''
        Teste si une portion de texte est sélectionnée ou non
        dans le document courant
        '''
        if not xIndexAccess.supportsService('com.sun.star.text.TextRanges'):
            return True
        count = xIndexAccess.getCount()
        for i in range(xIndexAccess.getCount()):
            xTextRange = xIndexAccess.getByIndex(i)
            xCursor = xTextRange.getText().createTextCursorByRange(xTextRange)
            if xCursor.isCollapsed():
                count -= 1
            del xCursor
        return (count > 0)

    def __walk_texttable(self, oTableCursor):
        '''
        Traitement d'un TextTable (tableau) inséré dans le document courant
        '''
        pass
        # cellRangeName = oTableCursor.getRangeName()
        # startColumn = ord(cellRangeName.split(':')[0][0])
        # endColumn = ord(cellRangeName.split(':')[1][0])
        # startRow = ord(cellRangeName.split(':')[0][1])
        # endRow = ord(cellRangeName.split(':')[1][1])
        #
        # for col in range(startColumn, endColumn):
        #     for row in range(startRow, endRow):
        #         oTableCursor.gotoCellByName(chr(col)+chr(row), False)
        #         oTableCursor.gotoCellByName(chr(col)+chr(row), True)
        #         if oTableCursor.supportsService("com.sun.star.text.Paragraph"):
        #             self.__apply(oTableCursor)
        #         elif xCursor.supportsService('com.sun.star.text.TextTableCursor'):
        #             self.__walk_texttable(oTableCursor)
        #         elif oTableCursor.supportsService('com.sun.star.text.TextRanges'):
        #             self.__walk_textranges(oTableCursor)

    def __walk_one_textrange(self, xTextRange, xLCursorLimit, xRCursorLimit):
        '''
        Traitement d'un TextRange (paragraphe)
        '''
        oTextElementEnum = xTextRange.createEnumeration()
        while oTextElementEnum.hasMoreElements():
            if self.is_stopped(): return
            oTextElement = oTextElementEnum.nextElement()
            if oTextElement.supportsService("com.sun.star.text.Paragraph"):
                self.__set_profile(oTextElement, xLCursorLimit, xRCursorLimit)
            elif oTextElement.supportsService('com.sun.star.text.TextTableCursor'):
                self.__walk_texttable(oTextElement)

    def __walk_textranges(self, xIndexAccess):
        '''
        Traitement de la succession des éléments sélectionnés
        dans le document courant
        '''
        count = xIndexAccess.getCount()
        for i in range(count):
            if self.is_stopped(): return
            xTextRange = xIndexAccess.getByIndex(i)
            xRCursorLimit = self.__get_right_most_cursor(xTextRange)
            xLCursorLimit = self.__get_left_most_cursor(xTextRange)
            self.__walk_one_textrange(xTextRange, xLCursorLimit, xRCursorLimit)

    def __get_left_most_cursor(self, oSel):
        '''  Récupération du curseur le plus à gauche
        dans le paragraphe indiqué
        '''
        oCursor = oSel.getText().createTextCursorByRange(oSel)
        oCursor.collapseToStart()
        return oCursor

    def __get_right_most_cursor(self, oSel):
        '''  Récupération du curseur le plus à droite
        dans le paragraphe indiqué
        '''
        oCursor = oSel.getText().createTextCursorByRange(oSel)
        oCursor.collapseToEnd()
        return oCursor

    def is_stopped(self):
        return self._stopped

    def stop(self):
        '''
        Ordre d'arrêt du processus
        '''
        self._stopped = True
        try:
            self._ui.endExecute()
        except:
            pass

    def execute(self, xDocument):
        # walk along the (selected) text
        xSelectionSupplier = xDocument.getCurrentController()
        xIndexAccess = xSelectionSupplier.getSelection()
        if not self.__is_anything_selected(xIndexAccess):
            ''' Process the whole document '''
            xText = xDocument.getText()
            xCursor = xText.createTextCursor()
            xCursor.gotoStart(False)
            xCursor.gotoEnd(True)
            xLCursorLimit = xText.createTextCursorByRange(xCursor)
            xLCursorLimit.collapseToStart()
            xRCursorLimit = xText.createTextCursorByRange(xCursor)
            xRCursorLimit.collapseToEnd()
            self.__walk_one_textrange(xCursor, xLCursorLimit, xRCursorLimit)
        else:
            ''' Process the selected part of the document '''
            if xIndexAccess.supportsService('com.sun.star.text.TextTableCursor'):
                self.__walk_texttable(xIndexAccess)
            elif xIndexAccess.supportsService('com.sun.star.text.TextRanges'):
                self.__walk_textranges(xIndexAccess)

        self.stop()

    def run(self, xDocument, uprofile):
        '''
        Fonction principale d'application d'un profil sur le document
        passé en paramètre
        '''
        if uprofile.is_void(): return
        self._uprofile = uprofile
        self._stopped = False

        # création boite de dialogue et exécution
        self._create_ui()
        t = Thread(target=self.execute, args=[xDocument])
        t.start()
        self._ui.execute()
        self._ui.dispose()

