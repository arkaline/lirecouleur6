# #!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
###############################################################################
# LireCouleur6 - tools to help with reading French language
#
# http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 0.0.1
#
# GNU General Public Licence (GPL) version 3
# https://www.gnu.org/licenses/gpl-3.0.en.html
###############################################################################
import uno
import os
import json
import codecs
from functionlc6 import (create_lc6_function, create_uno_struct, STYLE_CORRESPONDANCE)



class UserProfile:
    '''
    Define the user's profile structure
    https://wiki.openoffice.org/wiki/Documentation/DevGuide/Text/Formatting
    '''

    def __init__(self, xModel=None, filename=None):
        self._stopped = True
        self._functions = []
        if (not xModel is None) and (not filename is None):
            self.load(xModel, filename)

    # some getters
    def get_novice_reader(self):
        return self._profile['params']['novice_reader']
    novice_reader = property(get_novice_reader)

    def get_oral_syllable(self):
        return self._profile['params']['SYLLABES_ORALES']
    oral_syllable = property(get_oral_syllable)

    def get_closed_syllable(self):
        return self._profile['params']['SYLLABES_ECRITES']
    closed_syllable = property(get_closed_syllable)

    def get_standard_syllable(self):
        return self._profile['params']['SYLLABES_STD']
    standard_syllable = property(get_standard_syllable)

    def get_functions(self):
        return self._profile['process']
    functions = property(get_functions)

    def get_params(self):
        return self._profile['params']
    params = property(get_params)

    def is_void(self):
        return (self._functions is None)

    def get_name(self):
        if 'name' in self._profile:
            return self._profile['name']
        else:
            return ""
    name = property(get_name)

    def get_description(self):
        if 'description' in self._profile:
            return self._profile['description']
        else:
            return ""
    description = property(get_description)

    # import styles into the document
    def __create_character_style(self, xModel, style_name, style_dict):
        '''
        Create a new character style
        '''
        char_styles = xModel.getStyleFamilies().getByName('CharacterStyles')

        # then create the other character styles
        if len(style_name) > 0:
            if char_styles.hasByName(style_name):
                # get the already existing char style
                tmp_style = char_styles.getByName(style_name)
            else:
                # create a char style
                tmp_style = xModel.createInstance('com.sun.star.style.CharacterStyle')
            # insertion des propriétés dans le style de caractères
            for sty in STYLE_CORRESPONDANCE:
                if sty in style_dict:
                    val =  style_dict[sty]
                    try:
                       val = eval(style_dict[sty])
                    except:
                        pass
                    try:
                        tmp_style.setPropertyValue(STYLE_CORRESPONDANCE[sty], val)
                    except:
                        print(STYLE_CORRESPONDANCE[sty], ' ', style_dict[sty], ' ', val)
                        print('ECHOUÉ')
                        pass
            # si le style de caractère n'existe pas encore,
            # introduction du nouveau style dans les styles de caractères
            if not char_styles.hasByName(style_name):
                char_styles.insertByName(style_name, tmp_style)

    def __import_character_styles(self, xModel):
        '''
        Import (modify or create) character styles in the document
        '''
        for func in self._profile['process']:
            if 'format' in func:
                format = func['format']
                if isinstance(func['format'], dict):
                    for nf in format:
                        self.__create_character_style(xModel, nf, format[nf])

    def __import_paragraph_styles(self, xModel):
        '''
        Import modifications in the default paragraph style of the document
        '''
        if not 'format' in self._profile: return
        para_styles = xModel.getStyleFamilies().getByName('ParagraphStyles')
        char_styles = xModel.getStyleFamilies().getByName('CharacterStyles')
        if not para_styles.hasByName('Standard'): return
        if not char_styles.hasByName('Standard'): return
        para_styl = para_styles.getByName('Standard')
        char_styl = char_styles.getByName('Standard')

        for func in self._profile['format']:
            if func in STYLE_CORRESPONDANCE:
                if func == 'line_spacing':
                    pls = create_uno_struct('com.sun.star.style.LineSpacing')
                    pls.Mode = 0
                    pls.Height = eval(self._profile['format'][func])
                    para_styl.setPropertyValue(STYLE_CORRESPONDANCE[func], pls)
                else:
                    fff = STYLE_CORRESPONDANCE[func]
                    try:
                        para_styl.setPropertyValue(fff, eval(self._profile['format'][func]))
                    except:
                        pass
                    try:
                        char_styl.setPropertyValue(fff, eval(self._profile['format'][func]))
                    except:
                        pass

    def load(self, xModel, filename):
        '''
        Load a profile from a json file and import the
        character styles it contains
        '''
        try:
            # cleaning
            del self._profile
        except:
            pass
        try:
            # cleaning
            del self._functions
        except:
            pass

        self._profile = None
        self._functions = None
        if not os.path.exists(filename): return
        f = codecs.open(filename, "r", "utf_8_sig", errors="replace")
        self._profile = json.load(f)
        f.close()

        if not 'params' in self._profile:
            self._profile['params'] = {}
        if 'SYLLABES_ORALES' in self._profile['params']:
            self._profile['params']['SYLLABES_ECRITES'] = 1-self._profile['params']['SYLLABE_ORALES']
        else:
            if 'SYLLABES_ECRITES' in self._profile['params']:
                self._profile['params']['SYLLABES_ORALES'] = 1-self._profile['params']['SYLLABES_ECRITES']
            else:
                self._profile['params']['SYLLABES_ECRITES'] = 1
                self._profile['params']['SYLLABES_ORALES'] = 1-self._profile['params']['SYLLABES_ECRITES']

        if 'SYLLABES_STD' in self._profile['params']:
            self._profile['params']['SYLLABES_LC'] = 1-self._profile['params']['SYLLABES_STD']
        else:
            if 'SYLLABES_LC' in self._profile['params']:
                self._profile['params']['SYLLABES_STD'] = 1-self._profile['params']['SYLLABES_LC']
            else:
                self._profile['params']['SYLLABES_STD'] = 1
                self._profile['params']['SYLLABES_LC'] = 1-self._profile['params']['SYLLABES_STD']

        # once profile is loaded, import character styles
        self.__import_character_styles(xModel)

        # once profile is loaded, import paragraph styles
        self.__import_paragraph_styles(xModel)

        # create text processing functions
        self._functions = [create_lc6_function(func, self._profile['params']) for func in self._profile['process']]

    def is_stopped(self):
        return self._stopped

    def stop(self):
        self._stopped = True
        if not self.is_void():
            for func in self._functions:
                func.stop()

    def run(self, xCursor, xContext):
        if self.is_void(): return
        self._stopped = False
        self.execute(xCursor, xContext)
        self._stopped = True

    def execute(self, xCursor, xContext):
        for func in self._functions:
            if self._stopped: return
            xTmpCursor = xCursor.getText().createTextCursorByRange(xCursor)
            func.run(xTmpCursor, xContext)
            del xTmpCursor




class UnformatProfile(UserProfile):
    '''
    Profile pour retirer le formatage LireCouleur
    '''
    def __init__(self, xModel=None, filename=None):
        UserProfile.__init__(self, xModel, filename)

        self._stopped = True
        self._functions = [create_lc6_function({'function': 'defaut'})]
