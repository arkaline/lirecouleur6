# #!/usr/bin/env python
# -*- coding: UTF-8 -*-
from __future__ import unicode_literals
###############################################################################
# LireCouleur6 - tools to help with reading French language
#
# http://lirecouleur.arkaline.fr
#
# @author Marie-Pierre Brungard
# @version 0.0.1
#
# GNU General Public Licence (GPL) version 3
# https://www.gnu.org/licenses/gpl-3.0.en.html
###############################################################################
import uno
import unohelper
import inspect
import json
import codecs
import shutil
from com.sun.star.task import XJobExecutor
from com.sun.star.document import XEventListener
from com.sun.star.awt import (XActionListener, XItemListener)
from com.sun.star.style.VerticalAlignment import TOP
from userprofile import (UserProfile, UnformatProfile)
from processlc6 import ProcessLC6
import os

VERSION = "v1.0.0"
CURRENT_DIRECTORY = os.path.dirname(os.path.abspath(inspect.getframeinfo(inspect.currentframe()).filename))
DEFAULT_PROFILE_FILENAME = 'default_lc6_profile.ini'

def get_current_user_profile():
    filename = os.sep.join([CURRENT_DIRECTORY, DEFAULT_PROFILE_FILENAME])
    f = codecs.open(filename, "r", "utf_8_sig", errors="replace")
    lines = f.readlines()
    f.close()
    for line in lines:
        sline = line.strip()
        if len(sline) > 0:
            return sline

class ProfileSelectorDialog(unohelper.Base, XActionListener, XItemListener):
    '''
    Gestion de la boite de dialogue de sélection de profil
    '''
    def __init__(self, ctx, doc):
        __CURRENT_PATH = os.sep.join([CURRENT_DIRECTORY, 'appdata'])

        self._context = ctx
        self._document = doc
        self._list_prof = {}
        curprofilename = get_current_user_profile()
        for pp in os.listdir(__CURRENT_PATH):
            f = codecs.open(os.sep.join([__CURRENT_PATH, pp]), "r", "utf_8_sig", errors="replace")
            prof = json.load(f)
            f.close()
            self._list_prof[prof['name']] = {'file': os.sep.join([__CURRENT_PATH, pp]), 'desc': prof['description']}
            if curprofilename.endswith(pp):
                self._selprof = prof['name']

    def _create_uno_service(self, serviceName):
        sm = self._context.getServiceManager()
        try:
            return sm.createInstanceWithContext(serviceName, self._context)
        except:
            pass

        try:
            return sm.createInstance(serviceName)
        except:
            pass

    def _select_prof_ui_(self, profname):
        self._selprof = profname
        self.descprof.Text = self._list_prof[profname]['desc']

    def _create_ui(self):
        from com.sun.star.awt.PushButtonType import OK
        from com.sun.star.awt.PushButtonType import CANCEL
        LABEL_HEIGHT = 10
        MARGIN = 2
        XPOSITION = 190
        YPOSITION = 107
        MARGIN = 2
        LIST_HEIGHT = 12
        TEXTFIELD_HEIGHT = 24
        BUTTON_WIDTH = 40
        BUTTON_HEIGHT = 15
        HEIGHT = MARGIN*8 + BUTTON_HEIGHT + LIST_HEIGHT + TEXTFIELD_HEIGHT + LABEL_HEIGHT
        TITLE = "LireCouleur 6 - "+VERSION
        OKBUTTON = "Valider"
        CANCELBUTTON = "Fermer"
        FILEBUTTON = "Plus..."
        LBL1 = "Profil choisi"
        WIDTH = 3*(BUTTON_WIDTH+MARGIN) + MARGIN

        ctx = self._context
        smgr = ctx.getServiceManager()
        desktop = smgr.createInstanceWithContext( "com.sun.star.frame.Desktop",ctx)
        frame = desktop.getCurrentFrame()
        window = frame.getContainerWindow()
        toolkit = window.getToolkit()
        dialogModel = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialogModel", ctx)

        dialogModel.PositionX = XPOSITION
        dialogModel.PositionY = YPOSITION
        dialogModel.Width = WIDTH
        dialogModel.Height = HEIGHT
        dialogModel.Title = TITLE

        lbl1 = dialogModel.createInstance("com.sun.star.awt.UnoControlFixedTextModel")
        lbl1.PositionX = MARGIN
        lbl1.PositionY = MARGIN
        lbl1.Width  = WIDTH - 2*MARGIN
        lbl1.Height = LABEL_HEIGHT
        lbl1.Name = "lbl1"
        lbl1.Label = LBL1
        dialogModel.insertByName(lbl1.Name, lbl1)

        okbutton = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel")
        okbutton.Width  = BUTTON_WIDTH
        okbutton.Height = BUTTON_HEIGHT
        okbutton.PositionX = MARGIN
        okbutton.PositionY = dialogModel.Height-okbutton.Height-MARGIN
        okbutton.PushButtonType = OK
        okbutton.Label = OKBUTTON
        okbutton.Name = 'okbutton'
        dialogModel.insertByName(okbutton.Name, okbutton)

        self.listprof = dialogModel.createInstance("com.sun.star.awt.UnoControlComboBoxModel")
        self.listprof.Width  = WIDTH - 2*MARGIN
        self.listprof.Height  = LIST_HEIGHT
        self.listprof.Dropdown = True
        self.listprof.PositionX = (dialogModel.Width-self.listprof.Width)/2
        self.listprof.PositionY = lbl1.PositionY+lbl1.Height+MARGIN
        for k in self._list_prof.keys():
            self.listprof.insertItemText(0, k)
        self.listprof.Name = 'listprof'
        dialogModel.insertByName(self.listprof.Name, self.listprof)

        self.descprof = dialogModel.createInstance("com.sun.star.awt.UnoControlEditModel")
        self.descprof.Width = WIDTH - 2*MARGIN
        self.descprof.Height = TEXTFIELD_HEIGHT
        self.descprof.PositionX = (dialogModel.Width-self.descprof.Width)/2
        self.descprof.PositionY = self.listprof.PositionY+self.listprof.Height + MARGIN
        self.descprof.ReadOnly = True
        self.descprof.MultiLine = True
        self.descprof.Border = 0
        self.descprof.PaintTransparent = False
        self.descprof.VerticalAlign = TOP
        self.descprof.Name = 'descprof'
        dialogModel.insertByName(self.descprof.Name, self.descprof)
        self._select_prof_ui_(self._selprof)

        cancelbutton = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel")
        cancelbutton.Width  = BUTTON_WIDTH
        cancelbutton.Height = BUTTON_HEIGHT
        cancelbutton.PositionX = okbutton.PositionX+okbutton.Width+MARGIN
        cancelbutton.PositionY = okbutton.PositionY
        cancelbutton.Label = CANCELBUTTON
        cancelbutton.PushButtonType = CANCEL
        cancelbutton.Name = 'cancelbutton'
        dialogModel.insertByName(cancelbutton.Name, cancelbutton)

        filebtn = dialogModel.createInstance("com.sun.star.awt.UnoControlButtonModel")
        filebtn.Width  = BUTTON_WIDTH
        filebtn.Height = BUTTON_HEIGHT
        filebtn.PositionX = cancelbutton.PositionX+cancelbutton.Width+MARGIN
        filebtn.PositionY = cancelbutton.PositionY
        filebtn.Label = FILEBUTTON
        filebtn.Name = 'filebtn'
        dialogModel.insertByName(filebtn.Name, filebtn)

        self.controlContainer = smgr.createInstanceWithContext("com.sun.star.awt.UnoControlDialog", ctx)
        self.controlContainer.setModel(dialogModel)

        self.controlContainer.getControl(okbutton.Name).addActionListener(self)
        self.controlContainer.getControl(cancelbutton.Name).addActionListener(self)
        self.controlContainer.getControl(filebtn.Name).addActionListener(self)
        self.controlContainer.getControl(self.listprof.Name).addItemListener(self)
        self.controlContainer.getControl(self.listprof.Name).setText(self._selprof)

        self.controlContainer.setVisible(True)
        self.controlContainer.createPeer(toolkit, None)

        return self.controlContainer

    def get_selected_profile(self):
        return self._list_prof[self._selprof]['file']

    def itemStateChanged(self, _itemEvent):
        self._select_prof_ui_(self.controlContainer.getControl(self.listprof.Name).getText())

    def actionPerformed(self, _actionEvent):
        cmd = _actionEvent.Source.Model.Name

        if cmd == 'okbutton':
            self._selprof = self.controlContainer.getControl("listprof").getText()

            # fermeture de la boite de dialogue
            self.controlContainer.endExecute()
        elif cmd == 'cancelbutton':
            self.controlContainer.endExecute()
        elif cmd == 'filebtn':
            # create the dialog box to select the file
            ctx = self._context
            smgr = ctx.getServiceManager()
            oFilePicker = smgr.createInstanceWithContext("com.sun.star.ui.dialogs.FilePicker", ctx)
            oFilePicker.appendFilter("Documents", "*.json")
            oFilePicker.CurrentFilter = 'Documents'
            if oFilePicker.execute():
                sFiles = oFilePicker.getFiles()
                filename = uno.fileUrlToSystemPath(sFiles[0])
                f = codecs.open(filename, "r", "utf_8_sig", errors="replace")
                prof = json.load(f)
                f.close()
                try:
                    profname = prof['name']
                    __CURRENT_FILENAME = inspect.getframeinfo(inspect.currentframe()).filename
                    __CURRENT_PATH = os.sep.join([os.path.dirname(os.path.abspath(__CURRENT_FILENAME)), 'appdata'])

                    # copy profile file to appdata
                    shutil.copyfile(filename, os.sep.join([__CURRENT_PATH, os.path.basename(filename)]))
                    # add the new profile to the list of available profiles
                    self._list_prof[profname] = {'file': filename, 'desc': prof['description']}
                    if not profname in self._list_prof:
                        self.listprof.insertItemText(len(self._list_prof)-1, profname)

                    self.controlContainer.getControl(self.listprof.Name).setText(profname)
                    self._select_prof_ui_(profname)
                except:
                    pass
            oFilePicker.dispose()



def select_profile(args=None):
    '''
    Sélection d'un profil utilisateur et application de ce profil
    au texte sélectionné
    '''
    _select_profile(XSCRIPTCONTEXT.getDocument(), XSCRIPTCONTEXT.getComponentContext())

def _select_profile(xDocument, xContext):
    '''
    Fonction interne - sélection d'un profil utilisateur et
    application de ce profil au texte sélectionné
    '''
    if not xDocument.supportsService("com.sun.star.text.TextDocument"):
        return

    dlg = ProfileSelectorDialog(xContext, xDocument)
    _ui = dlg._create_ui()
    if _ui.execute():
        # copie du nom profil dans le fichier mémoire
        filename = os.sep.join([CURRENT_DIRECTORY, DEFAULT_PROFILE_FILENAME])
        f = codecs.open(filename, "w", "utf_8_sig", errors="replace")
        f.write(dlg.get_selected_profile())
        f.close()

        # lecture du profil
        uprofile = UserProfile(xDocument, dlg.get_selected_profile())

        # création et exécution du process
        proc = ProcessLC6(xContext)
        proc.run(xDocument, uprofile)
    _ui.dispose()

def format(args=None):
    '''
    Application du profil par défaut au texte sélectionné
    '''
    _format(XSCRIPTCONTEXT.getDocument(), XSCRIPTCONTEXT.getComponentContext())

def _format(xDocument, xContext):
    '''
    Fonction interne - application du profil par défaut au texte sélectionné
    '''
    if not xDocument.supportsService("com.sun.star.text.TextDocument"):
        return

    # lecture du nom du profil courant
    profilename = get_current_user_profile()
    if not os.path.exists(profilename):
        profilename = os.sep.join([CURRENT_DIRECTORY, profilename])

    # application du profil
    uprofile = UserProfile(xDocument, profilename)
    proc = ProcessLC6(xContext)
    proc.run(xDocument, uprofile)

def unformat(args=None):
    '''
    Application du profil par défaut au texte sélectionné
    '''
    _unformat(XSCRIPTCONTEXT.getDocument(), XSCRIPTCONTEXT.getComponentContext())

def _unformat(xDocument, xContext):
    '''
    Fonction interne - application du profil par défaut au texte sélectionné
    '''
    if not xDocument.supportsService("com.sun.star.text.TextDocument"):
        return

    proc = ProcessLC6(xContext)
    prof = UnformatProfile()
    proc.run(xDocument, prof)



class LC6(unohelper.Base, XJobExecutor):
    '''
    Point d'entrée principal du module
    '''
    def __init__(self, context):
        self.context = context

    def trigger(self, arg):
        smgr = self.context.getServiceManager()
        desktop = smgr.createInstanceWithContext('com.sun.star.frame.Desktop', self.context)
        if arg == 'select':
            _select_profile(desktop.getCurrentComponent(), self.context)
        elif arg == 'format':
            _format(desktop.getCurrentComponent(), self.context)
        else:
            _unformat(desktop.getCurrentComponent(), self.context)


"""
Export des fonctions appelables comme macro ou via une extension
"""
g_exportedScripts = format, select_profile, unformat,

g_ImplementationHelper = unohelper.ImplementationHelper()

g_ImplementationHelper.addImplementation(
    None,
    "org.openoffice.script.DummyImplementationForPythonScripts",
    ("org.openoffice.script.DummyServiceForPythonScripts",),)

g_ImplementationHelper.addImplementation(
    LC6, 'org.lirecouleur.lc6.impl', ())


"""
Ensemble de fonctions utilitaires -- à supprimer dans la version release
"""
if not 'XSCRIPTCONTEXT' in globals():
    try:
        import IDE_utils as office
        ctx = office.connect(host='localhost', port=2002, flush=True)
        XSCRIPTCONTEXT = office.ScriptContext(ctx)  # Adapt
    except:
        pass

def reload_lc6(args=None):
    try:
        global XSCRIPTCONTEXT
        import importlib
        from os.path import basename

        mainmodname = basename(__file__)[:-3]
        module = importlib.import_module(mainmodname)
        importlib.reload(module)
        globals().update(vars(module))

        ctx = office.connect(host='localhost', port=2002, flush=True)
        XSCRIPTCONTEXT = office.ScriptContext(ctx)  # Adapt
    except:
        pass

"""

"""
if __name__ == "__main__":
    # Windows : "C:\Program Files\LibreOffice\program\soffice.exe" "--accept=socket,host=localhost,port=2002,tcpNoDelay=1;urp;StarOffice.ComponentContext" --norestore
    # LINUX : soffice "--accept=socket,host=localhost,port=2002,tcpNoDelay=1;urp;StarOffice.ComponentContext" --norestore
    pass

